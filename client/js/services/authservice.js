angular
  .module('app')
  .factory('AuthService', ['User', '$q', '$rootScope', '$state', function(
      User, $q, $rootScope, $state) {
    function login(username, password) {
      return User
        .login({username:username,password:password })
        .$promise   
        .then(function(response) {
          $rootScope.currentUser = {
            id: response.user.id,
            tokenId: response.id,
            username: username
          };
        });
    }

    function logout() {
      return User
       .logout()
       .$promise
       .then(function() {
         $rootScope.currentUser = null;
       });
    }

    function register(username,realm,email, password) {
      return User
        .create({
        username:username,
        realm:realm,
        email: email,
        emailVerified: true,
        password: password
       })
       .$promise;
    }

    function refresh(accessTokenId) {
      return User
        .getCurrent(function(userResource) {
          $rootScope.currentUser = {
            id: userResource.id,
            tokenId: accessTokenId,
            email: userResource.email
          };
        });
    }
    return {
      login: login,
      logout: logout,
      register: register,
      refresh: refresh
    };
  }]);
angular
.module('app')
.factory('toastrService', function(){
    function success(message){
        document.getElementById("toastr").innerHTML="";
        var para = document.createElement("p");
        para.setAttribute('id','message');
        var textnode = document.createTextNode(message);
        para.appendChild(textnode); 
       var toaster =  document.getElementById("toastr")
       toaster.className = "show";
       toaster.appendChild(para);
       console.log(toaster)
       setTimeout(function(){ toaster.className = toaster.className.replace("show", ""); }, 2000);


    }
    return {
        success: success,
    }
})
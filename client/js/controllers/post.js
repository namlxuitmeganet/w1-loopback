angular
    .module('app')
    .controller('AllPostsController', ['$scope', 'Post', function ($scope,
        Post, toastr) {
        $scope.posts = Post.find({
            filter: {
                include: {
                    relation: 'user'
                }
            }
        })
        
        $scope.shortContent = function (str) {
            return str.substring(0, 200) + " ...";
        }
    }])
    .controller('CreatePostController', ['$scope', 'Post', '$state','toastrService',
        function ($scope, Post, $state,toastrService) {
            $scope.action = 'Create';
            $scope.submitForm = function () {
                console.log($scope.post.title);
                Post
                    .create({
                        title: $scope.post.title,
                        content: $scope.post.content
                    })
                    .$promise
                    .then(function () {
                        toastrService.success("Create Post success");
                        $state.go("all-posts");
                    })
            }
        }])
    .controller('MyPostController', ['$scope', 'Post',
        function ($scope, Post) {
            console.log($scope.currentUser.id);
            // after a refresh, the currenUser is not immediately on the scope
            // So, we're watching it on the scope and load my reviews only then.
            $scope.$watch('currentUser.id', function (value) {
                if (!value) {
                    return;
                }
                $scope.posts = Post.find({
                    filter: {
                        where: {
                            publisherId: $scope.currentUser.id
                        },
                        include: {
                            relation: 'user'
                        }
                    }
                });
            });
            $scope.shortContent = function (str) {
                return str.substring(0, 200) + " ...";
            }
            
        }])
    .controller('EditPostController', ['$scope', '$q', 'Post',
        '$stateParams', '$state', 'toastrService', 
        function ($scope, $q, Post,
            $stateParams, $state,toastrService) {
            $scope.action = "Edit";
            $scope.isDisabled = true;
            $q
                .all([
                    Post.findById({ id: $stateParams.id }).$promise
                ])
                .then(function (data) {
                    $scope.post = data[0];
                })
            $scope.submitForm = function () {
                $scope.post
                    .$save()
                    .then(function (post) {
                        toastrService.success("Edit Post Success");
                        $state.go('all-posts')
                    })
            }
        }])
    .controller('ViewPostController', ['$scope', '$q', 'Post',
        '$stateParams', '$state', function ($scope, $q, Post,
            $stateParams, $state) {
                ;
            $scope.isDisabled = true;
            $q
                .all([
                    Post.findById({ id: $stateParams.id }).$promise
                ])
                .then(function (data) {
                    $scope.post = data[0];
                })
        }])

    .controller('DeletePostController', ['$scope', 'Post', '$state',
        '$stateParams','toastrService', function ($scope, Post, $state, $stateParams,toastrService) {
            Post
                .deleteById({ id: $stateParams.id })
                .$promise
                .then(function () {
                    $state.go('mypost');
                    toastrService.success("Delete success");
                });
        }])




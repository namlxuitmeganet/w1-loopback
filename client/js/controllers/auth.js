
angular
  .module('app' )
  .controller('AuthLoginController', ['$scope', 'AuthService','$state','toastrService',
    function($scope, AuthService,$state,toastrService) {
      
      $scope.login = function() {
        
        AuthService.login($scope.user.username, $scope.user.password)
          .then(function() {
              // return to saved returnTo state before redirection to login
          if ($scope.returnTo && $scope.returnTo.state) {
            $state.go(
              $scope.returnTo.state.name,
              $scope.returnTo.params
            );
            // maintain the inherited rootscope variable returnTo
            // but make the returnTo state of it null,
            // so it can be used again after a new login.
            $scope.returnTo.state  = null;
            $scope.returnTo.params = null;
            return;
          }
            toastrService.success("Login Success");
            $state.go('all-posts');
          });

         
      };
    }
  ])
  .controller('AuthLogoutController', ['$scope', 'AuthService', '$state','toastrService',
    function($scope, AuthService, $state,toastrService) {
      AuthService.logout()
        .then(function() {
          toastrService.success("Logout success");
          $state.go('all-posts');
        });
    }
  ])
  .controller('SignUpController', ['$scope', 'AuthService', '$state','toastrService',
    function($scope, AuthService, $state,toastrService) {
      $scope.register = function() {
        AuthService.register($scope.user.username,$scope.user.realm,$scope.user.email, $scope.user.password)
          .then(function() {
            toastrService.success("Signup success");  
            $state.go('login');
          });
      };
    }
  ]);
// Copyright IBM Corp. 2015. All Rights Reserved.
// Node module: loopback-getting-started-intermediate
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

angular
  .module('app', [
    'ui.router',
    'lbServices',
  ])
  .config(['$stateProvider', '$urlRouterProvider', function($stateProvider,
      $urlRouterProvider) {
    $stateProvider
    
      .state('all-posts', {
        url: '/all-posts',
        templateUrl: 'views/allposts.html',
        controller: 'AllPostsController'
      })
      .state('login', {
        url: '/login',
        templateUrl: 'views/login.html',
        controller:'AuthLoginController'
      })
      .state('createpost',{
        url: '/creatpost',
        templateUrl: 'views/createpost.html',
        controller:'CreatePostController'
      })
      .state('logout', {
        url: '/logout',
        controller:'AuthLogoutController'
      })
      .state('mypost', {
        url: '/mypost',
        templateUrl: 'views/mypost.html',
        controller:'MyPostController'
      })
      .state('edit-post', {
        url: '/edit-post/:id',
        templateUrl: 'views/createpost.html',
        controller: 'EditPostController',
        authenticate: true
      })
      .state('view-post', {
        url: '/view-post/:id',
        templateUrl: 'views/viewpost.html',
        controller: 'ViewPostController',
        authenticate: true
      })
      .state('delete-post', {
        url: '/delete-post/:id',
        controller: 'DeletePostController',
        authenticate: true
      })
      .state('signup', {
        url: '/signup',
        templateUrl: 'views/signup.html',
        controller: 'SignUpController',
      })
    $urlRouterProvider.otherwise('all-posts');
  }])
  .run(['$rootScope', '$state', 'LoopBackAuth', 'AuthService', function($rootScope, $state,LoopBackAuth, AuthService) {
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
      // redirect to login page if not logged in
      if (toState.authenticate && !LoopBackAuth.accessTokenId) {
        event.preventDefault(); //prevent current page from loading

        // Maintain returnTo state in $rootScope that is used
        // by authService.login to redirect to after successful login.
        // http://www.jonahnisenson.com/angular-js-ui-router-redirect-after-login-to-requested-url/
        $rootScope.returnTo = {
          state: toState,
          params: toParams
        };

        $state.go('forbidden');
      }
      
    });
    if (LoopBackAuth.accessTokenId && !$rootScope.currentUser) {
      AuthService.refresh(LoopBackAuth.accessTokenId);
    }
  }]);
